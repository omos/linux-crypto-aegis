#!/bin/bash

rmmod aegis_test.ko 2>/dev/null
rmmod aegis256-aesni.ko 2>/dev/null
rmmod aegis256.ko 2>/dev/null
rmmod aegis128l-aesni.ko 2>/dev/null
rmmod aegis128l.ko 2>/dev/null
rmmod aegis128-aesni.ko 2>/dev/null
rmmod aegis128.ko 2>/dev/null
