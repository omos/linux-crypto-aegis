obj-m += aegis128.o aegis128l.o aegis256.o aegis_test.o

ifeq ($(CONFIG_X86_64),y)
	aegis128-aesni-objs += aegis128-aesni-asm.o aegis128-aesni-glue.o
	aegis128l-aesni-objs += aegis128l-aesni-asm.o aegis128l-aesni-glue.o
	aegis256-aesni-objs += aegis256-aesni-asm.o aegis256-aesni-glue.o

	obj-m += aegis128-aesni.o aegis128l-aesni.o aegis256-aesni.o
endif

ifeq ($(KERNEL_BUILD),)
	ifeq ($(KERNEL_VERSION),)
		KERNEL_VERSION=$(shell uname -r)
	endif
	KERNEL_BUILD=/lib/modules/$(KERNEL_VERSION)/build
endif

all:
	make -C $(KERNEL_BUILD) M=$(PWD) modules

clean:
	make -C $(KERNEL_BUILD) M=$(PWD) clean
