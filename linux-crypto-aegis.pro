TEMPLATE = lib
CONFIG -= qt
CONFIG += staticlib

DEFINES += __KERNEL__

ARCH=x86
SRC_PROJECT_PATH = $$PWD
LINUX_VERSION = $$system(uname -r)
LINUX_HEADERS_PATH = /lib/modules/$$LINUX_VERSION/build

INCLUDEPATH += $$LINUX_HEADERS_PATH/include
INCLUDEPATH += $$LINUX_HEADERS_PATH/arch/$$ARCH/include

buildmod.commands = make -C $$LINUX_HEADERS_PATH M=$$SRC_PROJECT_PATH modules
cleanmod.commands = make -C $$LINUX_HEADERS_PATH M=$$SRC_PROJECT_PATH clean
QMAKE_EXTRA_TARGETS += buildmod cleanmod

HEADERS += \
    aegis.h \
    aegis_tv.h

SOURCES += \
    aegis_test.c \
    aegis128.c \
    aegis128-aesni-asm.S \
    aegis128-aesni-glue.c \
    aegis128l.c \
    aegis128l-aesni-asm.S \
    aegis128l-aesni-glue.c \
    aegis256.c \
    aegis256-aesni-asm.S \
    aegis256-aesni-glue.c

DISTFILES += \
    Makefile
