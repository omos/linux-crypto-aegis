#!/bin/bash

modprobe cryptd

insmod aegis128.ko || exit 1
insmod aegis128-aesni.ko
insmod aegis128l.ko || exit 1
insmod aegis128l-aesni.ko
insmod aegis256.ko || exit 1
insmod aegis256-aesni.ko
insmod aegis_test.ko || exit 1
